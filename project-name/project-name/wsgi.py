"""
WSGI config for project_name project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project-name.settings")

if 'OPENSHIFT_REPO_DIR' in os.environ:
    if 'PROJECT_NAME' in os.environ:
        project_name = os.environ['PROJECT_NAME']
        print("PROJECT_NAME:", project_name, "found")
    else:
        project_name = os.environ['OPENSHIFT_APP_NAME']
    os.environ['DJANGO_SETTINGS_MODULE'] = project_name + '.settings'
    sys.path.append(os.path.join(os.environ['OPENSHIFT_REPO_DIR'], project_name))

    virtenv = os.environ['VIRTUAL_ENV']
    os.environ['PYTHON_EGG_CACHE'] = os.path.join(virtenv, 'lib/python3.3/site-packages')
    virtualenv = os.path.join(virtenv, 'bin/activate_this.py')

    try:
        exec_namespace = dict(__file__=virtualenv)
        with open(virtualenv, 'rb') as exec_file:
            file_contents = exec_file.read()
            compiled_code = compile(file_contents, virtualenv, 'exec')
            exec(compiled_code, exec_namespace)
    except IOError as err:
        print("IOError from wsgi")
        print(err)

application = get_wsgi_application()
