#!/bin/sh
old_name='project-name'
new_name=${1}
echo New Name: ${new_name}
# Recursively renaming nested directories of the same name seems awful difficult
# Cheat by knowing the exact ones - yes, this is very fragile
rename -v "s/${old_name}/${new_name}/" ${old_name}
rename -v "s/${old_name}/${new_name}/" ${new_name}/${old_name}
grep -rl ${old_name} .config .openshift ${new_name} | xargs sed -i "s/${old_name}/${new_name}/g"

