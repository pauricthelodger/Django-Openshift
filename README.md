Django Openshift
================

## About

This is to help you get started with making a [Django](https://djangoproject.com) project on [OpenShift](https://www.openshift.com/).
We assume you're planning to use Django 1.8 on a Python 3.3 gear on Openshift and we'll cover the general steps for that.

### Licence

The code is licenced under the MIT licence and is based on work done on https://github.com/pauricthelodger/openshift-mezzanine-py3.

## Getting Started

* Create an account on [OpenShift](https://www.openshift.com/)
    * Install [`rhc`](https://developers.openshift.com/en/managing-client-tools.html), the OpenShift command line tool
    * Follow the setup steps for `rhc` but don't create an app just yet
* Choose an app domain on OpenShift for the new project - this is basically the app group
    * The default url for you becomes: your_app_name-your_domain_name.rhcloud.com
* Make sure [virtualenv](http://virtualenv.readthedocs.org/) is installed on your local machine
* Make sure Python 3.3 is installed on your local machine
    * Python 3.3 is the most recent available on OpenShift at the time of writing
    * We're assuming you're running Ubuntu for this example so:
        * Add the [deadsnakes ppa](https://launchpad.net/~fkrull/+archive/ubuntu/deadsnakes/)

## Setup

Replace all instances of 'project-name' for your directories and the examples below

    # Create and activate a virtualenv for your project
    # Always activate before you run commands on your django project
    virtualenv -p /usr/bin/python3.3 project-name
    cd project-name
    source bin/activate

    # Create application
    # To enable auto-scaling, append -s to command
    rhc app create project-name python-3.3 postgresql-9.2
    cd project-name

    # add this repo as a remote (you can remove again after setup)
    git remote add django_openshift -m master repo_url_for_django_openshift

    # copy the layout of the django_openshift project
    git pull -s recursive -X theirs django_openshift master

    # this finds and swaps in your real project name in all those new files and filenames
    ./name_replacer.sh project-name

Set up some environment variables for OpenShift

    # You don't need to set PROJECT_NAME if you are using the same name for the OpenShift app
    # The deploy script will choose the app name instead if so
    rhc env set PROJECT_NAME=project-name

    # We're assuming this is your DEV instance
    rhc env set DEBUG=True

    # set the requirements file location
    rhc env set OPENSHIFT_PYTHON_REQUIREMENTS_PATH=.config/deploy/DEV/requirements.txt

    # set the wsgi file location
    rhc env set OPENSHIFT_PYTHON_WSGI_APPLICATION=project-name/project-name/wsgi.py
    
Alternatively, you can use a local config file so they can be read all at once

    # Here's one we made earlier
    # By now the name replacer will have filled it out with the correct name
    # "-a" is this is your app name
    rhc set-env .config/deploy/DEV/openshift_envvars -a project-name

---
    
    # .config/deploy/DEV/openshift_envvars
    PROJECT_NAME=project-name
    DEBUG=True
    OPENSHIFT_PYTHON_REQUIREMENTS_PATH=.config/deploy/DEV/requirements.txt
    OPENSHIFT_PYTHON_WSGI_APPLICATION=project-name/project-name/wsgi.py

---

For local testing, you need to add PROJECT_NAME and DEBUG
to the activate script of your virtualenv

    echo "export PROJECT_NAME=project-name" >> $VIRTUAL_ENV/bin/activate
    echo "export DEBUG=True" >> $VIRTUAL_ENV/bin/activate
    source $VIRTUAL_ENV/bin/activate

Finally, enter the details of your project in the setup.py file. 

## Deployment

    # Commit changes and push to OpenShift
    git add -A
    git commit -m "Commit initial project code"
    git push

### First Deployment

You'll need to create an admin user on the OpenShift instance

    rhc ssh project-name
    source $VIRTUAL_ENV/bin/activate
    # this will ask you for a username, email address, and password
    python $OPENSHIFT_REPO_DIR/$PROJECT_NAME/manage.py createsuperuser

That's everything. Go make something awesome!

## Useful Links

* [Django Docs](http://docs.djangoproject.com/)
* [Openshift Docs](https://developers.openshift.com/)

