#!/usr/bin/env python

from setuptools import setup

setup(
    name='Project Name',
    version='0.1',
    description='Project Description',
    author='Project Author',
    author_email='Project Email',
    url='Project URL',
    )

